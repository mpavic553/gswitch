using UnityEngine;




public class StorageManager
{


    public static float DEFAULT_SOUND = 0.5f;
    public static void StoreMusicVolume(float volume)
    {
        PlayerPrefs.SetFloat(StorageTags.VOLUME_MUSIC, volume);
    }
    public static void StoreSFXVolume(float volume)
    {
        PlayerPrefs.SetFloat(StorageTags.VOLUME_SFX, volume);
    }

    public static float GetMusicVolume()
    {
        if (PlayerPrefs.HasKey(StorageTags.VOLUME_MUSIC))
        {
            return PlayerPrefs.GetFloat(StorageTags.VOLUME_MUSIC);
        }

        return DEFAULT_SOUND;
    }
    public static float GetSFXVolume()
    {
        if (PlayerPrefs.HasKey(StorageTags.VOLUME_SFX))
        {
            return PlayerPrefs.GetFloat(StorageTags.VOLUME_SFX);
        }
        return DEFAULT_SOUND;
    }
}