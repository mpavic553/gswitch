﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour, ILevelGenerateHandler,IPlayerHandler,IEnemyGenerateHandler
{
    public GameObject levelGen;
    public GameObject enemyGen;
    public GameObject UiManagerObject;
    private LevelGenerator levelGenerator;
    private EnemyGenerator enemyGenerator;
    private IUIManage UiManager;
    private Hero heroController;
    GameObject player;

    private int enemyGenCount = GameRules.INITIAL_ENEMY_COUNT;
    private int enemyWorth = GameRules.INITIAL_ENEMY_WORTH;

    private int PlayerPoints = 0;
    private int CurrentWave = 1;

    public bool IsGameOver { get; private set; }

    void Awake(){
        UiManager = UiManagerObject.GetComponent<UIManager>();

    }
    public void LevelGenerated()
    {
        if(player == null){
            LoadPlayerPrefab();
        }
        SpawnPlayer();
        enemyGenerator.GenerateEnemies(enemyGenCount);
    }
    
    private void LoadPlayerPrefab()
    {
        player = Resources.Load(StaticRes.HERO_PATH) as GameObject;    
    }

    private void SpawnPlayer()
    {
        int verticalLimit = levelGenerator.heightInUnits /3;
        int horizontallimit =levelGenerator.widthInUnits /3;
        int x = UnityEngine.Random.Range(horizontallimit,horizontallimit + 10);
        int y = UnityEngine.Random.Range(verticalLimit,verticalLimit + 10);

        player = Instantiate(player,new Vector3(x,y,this.transform.position.z),this.transform.rotation) as GameObject;
        heroController = player.GetComponent<Hero>();
        heroController.PlayerHandler = this;
        heroController.maxX = levelGenerator.widthInUnits;
        heroController.maxY = levelGenerator.heightInUnits;
    }

    // Start is called before the first frame update
    void Start()
    {
           Init();
        levelGenerator.LevelGenerateHandler = this;
        enemyGenerator.enemyGenerateHandler = this;
        levelGenerator.GenerateLevel();
    }
        private void Init()
    {
        levelGenerator = levelGen.GetComponent<LevelGenerator>();
        enemyGenerator = enemyGen.GetComponent<EnemyGenerator>();

        UiManager.PrintWave(CurrentWave);
        UiManager.PrintScore(PlayerPoints);
    }
    public void AttackEnemyDetected(bool shouldDestroy,GameObject col)
    {
        if(shouldDestroy){
            SoundManager.Instance.PlaySFX(Sounds.HIT_HERO);
            GameObject particleStystem = ObjectPooler.instance.GetObjectFromPool(Tags.PARTICLE_SYSTEM);
            particleStystem.transform.position = col.gameObject.transform.position;
            particleStystem.SetActive(true);
            col.gameObject.SetActive(false);
            PlayerPoints += enemyWorth;
            AwardPlayer();
            enemyGenerator.EnemyDelete();
        }
        if(col.gameObject.tag != Tags.BOSS){
             enemyGenerator.CountEnemies();
        }
    }
    void Update(){
        if(Input.GetKeyDown(KeyCode.R)){
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void EnemiesGone()
    {
        if(CurrentWave % GameRules.BOSS_TRESHHOLD == 0){
             SoundManager.Instance.PlaySound(Sounds.OST2);
        }
        CurrentWave++;
        if(CurrentWave % GameRules.BOSS_TRESHHOLD == 0){
            enemyGenerator.SpawnBoss();
            UiManager.PrintBossRound();
            SoundManager.Instance.PlaySound(Sounds.CRAB_OST);
        }else   {
        enemyGenCount += GameRules.INCREMENT_PER_WAVE;
        enemyWorth += GameRules.INCREMENT_ENEMY_WORTH;
        enemyGenerator.GenerateEnemies(enemyGenCount);
        UiManager.PrintWave(CurrentWave);
        }
    }

    public void EnemiesRemaining(int remainingEnemies)
    {
       UiManager.PrintEnemyCount(remainingEnemies);
    }

    public void EnemyDetected(bool shouldDestroy, GameObject col)
    {
        if(shouldDestroy){
            enemyGenerator.RemoveEnemies();
            Destroy(player);
            IsGameOver = true;
            ObjectPooler.instance.Clear();
            GameOver();
            SoundManager.Instance.PlaySFX(Sounds.DIE);
            
        } 
    }
    private void AwardPlayer(){
        HighScoreManager.Instance.NewScore(PlayerPoints);
        UiManager.PrintScore(PlayerPoints);
    }
    private void GameOver(){
        UiManager.PrintGameOver();
    }
}
