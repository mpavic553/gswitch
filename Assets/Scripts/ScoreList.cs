﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ScoreList : MonoBehaviour
{
    public GameObject playerScoreEntry;

    // Start is called before the first frame update
    void Start()
    {
      var ScoreBoard = HighScoreManager.Instance.GetScoreBoard().OrderByDescending((k)=>k.Value.Points).ToList();


      foreach (var key in ScoreBoard)
        {
            
            Score Score = key.Value;

            MakeEntry(Score);
        }
    }

    private void MakeEntry(Score Score)
    {
        GameObject entry = Instantiate(playerScoreEntry, this.transform.position, Quaternion.identity, this.transform);

        entry.transform.GetChild(0).GetComponent<Text>().text = Score.Name;
        entry.transform.GetChild(1).GetComponent<Text>().text = Score.Points.ToString();
    }
}
