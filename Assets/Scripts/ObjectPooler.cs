﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class ObjectPooler : MonoBehaviour
{

    List<GameObject> pool;

    public GameObject[] enemyPrefabs;
    public GameObject[] projectilePrefabs;
    public GameObject[] particleSystems;
    public static ObjectPooler instance;


    void Awake()
    {
        if (!instance)
        {
            instance = this;
            pool = new List<GameObject>();
            GenerateEnemyPool();
            GenerateProjectilePool();
            GenerateParticleSystems();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void GenerateParticleSystems(int count = 50)
    {
        for (int i = 0; i < count; i++)
        {

            GameObject particleSystem = Instantiate(particleSystems[0], Vector3.zero, Quaternion.identity, this.transform);

            pool.Add(particleSystem);
            particleSystem.SetActive(false);

        }
    }

    private void GenerateProjectilePool(int count = 20)
    {
        for (int i = 0; i < count; i++)
        {

            GameObject projectile = Instantiate(projectilePrefabs[0], Vector3.zero, Quaternion.identity, this.transform);

            pool.Add(projectile);
            projectile.SetActive(false);

        }

    }

    private void GenerateEnemyPool(int count = 50)
    {

        for (int i = 0; i < count; i++)
        {
            int enemyTypeIndex = UnityEngine.Random.Range(0, enemyPrefabs.Length);

            GameObject enemy = Instantiate(enemyPrefabs[enemyTypeIndex], Vector3.zero, Quaternion.identity, this.transform);
            pool.Add(enemy);
            enemy.SetActive(false);
        }


    }
    private void GeneratePool(string tag, int amount)
    {
        switch (tag)
        {
            case Tags.ENEMY:
                GenerateEnemyPool(amount);
                break;
            case Tags.PROJECTILE:
                GenerateProjectilePool(amount);
                break;
            default:
                GenerateEnemyPool();
                GenerateProjectilePool();
                break;

        }

    }

    public List<GameObject> GetObjectsFromPool(int count, Func<GameObject, bool> predicate)
    {

        List<GameObject> gameObjectList = pool.Where(predicate).Take(count).ToList();

        if (gameObjectList.Count < count || gameObjectList.Count == 0)
        {
            GeneratePool(tag, 1);
            return GetObjectsFromPool(count, predicate);
        }
        else
        {
            return gameObjectList;
        }
    }
    public GameObject GetObjectFromPool(string tag)
    {
        GameObject obj = pool.Where((obje) => { return !obje.activeInHierarchy && obje.tag == tag; }).FirstOrDefault();

        if (obj == null)
        {
            GeneratePool(tag, 1);
            return GetObjectFromPool(tag);
        }
        else
        {
            return obj;
        }
    }

    public void Clear()
    {
        pool.ForEach((item) =>
        {
            item.SetActive(false);
        });
    }
}
