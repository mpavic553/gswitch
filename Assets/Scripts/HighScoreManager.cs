﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class HighScoreManager : MonoBehaviour
{

    private IDictionary<string, Score> ScoreBoard;

    int CurrentScoreCount;
    public static HighScoreManager Instance;

    // Start is called before the first frame update
    private void Awake()
    {
        if (!Instance)
        {

            Instance = this;
            LoadScoreBoard();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }


    }


    public void SaveScore(string nickName)
    {

        Score score = new Score(nickName, CurrentScoreCount);
        ScoreBoard.Add(nickName, score);
    }

    public void LoadScoreBoard()
    {
        var path = Application.persistentDataPath + "/ScoreBoard.dat";
        if (File.Exists(path))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(path, FileMode.Open);
            ScoreBoard = (IDictionary<string, Score>)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            ScoreBoard = new Dictionary<string, Score>();
        }
    }
    public void SaveChanges()
    {
        var path = Application.persistentDataPath + "/ScoreBoard.dat";
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/ScoreBoard.dat");
        bf.Serialize(file, ScoreBoard);
        file.Close();
    }

    public IDictionary<string, Score> GetScoreBoard()
    {
        return this.ScoreBoard;
    }

    public void NewScore(int score)
    {
        CurrentScoreCount = score;
    }

}
