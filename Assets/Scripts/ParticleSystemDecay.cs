﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemDecay : MonoBehaviour
{
    void Die()
    {
        ParticleSystem.MainModule system = gameObject.GetComponent<ParticleSystem>().main;
        system.startColor = Color.red;
        this.gameObject.SetActive(false);
    }
    void OnEnable()
    {
        Invoke("Die", 3f);
    }
}
