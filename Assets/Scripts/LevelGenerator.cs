﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevelGenerateHandler
{
    void LevelGenerated();
}

public class LevelGenerator : MonoBehaviour
{
    public ILevelGenerateHandler LevelGenerateHandler { private get; set; }
    private int camera_maxX;
    private int camera_maxY;

    public int tileSize = 16;
    public int widthInUnits;
    public int heightInUnits;
    public int groundheight;
    public float platformGenerationFrequency = 0.1f;
    public GameObject platform;
    public GameObject decoration;

    public GameObject floatingPlatform;
    public GameObject blank;
    void Start()
    {

    }
    private void CalculateCamera()
    {

        var height = 2 * Camera.main.orthographicSize;
        var width = height * Camera.main.aspect;

        widthInUnits = (int)width + 1;
        heightInUnits = (int)height + 1;



    }
    public void GenerateLevel()
    {
        CalculateCamera();

        for (int x = 0; x < widthInUnits; x++)
        {
            for (int y = 0; y < heightInUnits; y++)
            {
                if (y == 1 || y == 0)
                {
                    Instantiate(blank, new Vector3(x, y, this.transform.position.z), this.transform.rotation).transform.parent = this.transform;
                }
                else if (y == 2)
                {
                    Instantiate(platform, new Vector3(x, y, this.transform.position.z), this.transform.rotation).transform.parent = this.transform;
                }
                else if (y == 3)
                {
                    Instantiate(decoration, new Vector3(x, y, this.transform.position.z), this.transform.rotation).transform.parent = this.transform;
                }
                else if (y == heightInUnits - 1)
                {
                    Instantiate(platform, new Vector3(x, y, this.transform.position.z), Quaternion.Euler(0, 0, -180)).transform.parent = this.transform;
                }
                else if (y == heightInUnits - 2)
                {
                    Instantiate(decoration, new Vector3(x, y, this.transform.position.z), Quaternion.Euler(0, 0, -180)).transform.parent = this.transform;
                }
                else
                {
                    MaybeGeneratePlatform(x, y);
                }
            }
        }

        Vector3 cameraMiddle = new Vector3(this.transform.position.x + (widthInUnits / 2) - 1, this.transform.position.y + (heightInUnits / 2));
        Camera.main.transform.Translate(cameraMiddle);
        LevelGenerateHandler.LevelGenerated();
    }
    private void MaybeGeneratePlatform(int x, int y)
    {
        float shouldGenerate = UnityEngine.Random.Range(0f, 100f);

        if (shouldGenerate < platformGenerationFrequency)
        {
            GeneratePlatform(x, y);
        }
    }

    private void GeneratePlatform(int x, int y)
    {
        Instantiate(floatingPlatform, new Vector3(x, y, this.transform.position.z), this.transform.rotation).transform.parent = this.transform;
        Instantiate(floatingPlatform, new Vector3(x + 1, y, this.transform.position.z), Quaternion.Euler(0, -180, 0)).transform.parent = this.transform;
    }
}
