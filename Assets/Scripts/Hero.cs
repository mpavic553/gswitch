﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerHandler
{
    void AttackEnemyDetected(bool shouldDestroy, GameObject col);
    void EnemyDetected(bool shouldDestroy, GameObject col);
}
public class Hero : MonoBehaviour
{
    public IPlayerHandler PlayerHandler { get; set; }
    private float speed = 20f;
    private Animator animator;
    private GameObject arrow;
    private Collider2D attackColider;
    public GameObject shootPoint;

    public int maxX = 0;
    public int maxY = 0;
    private int direction = 0;
    private bool gravityDown = true;
    // Start is called before the first frame update

    void Awake()
    {
        animator = GetComponent<Animator>();
        arrow = Resources.Load(StaticRes.ARROW) as GameObject;
        attackColider = this.transform.GetChild(0).GetComponent<BoxCollider2D>();
        attackColider.enabled = false;
    }
    void Start()
    {
        Time.timeScale = 0;
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            animator.SetTrigger("Attack");
        }
        if (Input.GetButtonDown("Fire2"))
        {
            animator.SetTrigger("AttackBow");
        }
        float translation = Input.GetAxis("Horizontal") * speed;
        MovePlayer(translation);
        CheckJump();
    }

    private void CheckJump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            SoundManager.Instance.PlaySFX(Sounds.SWITCH_GRAVITY);
            Physics2D.gravity *= -1;
            float physicsY = Physics2D.gravity.y;
            if (physicsY < 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
                gravityDown = true;
            }
            else
            {
                transform.rotation = Quaternion.Euler(0, 0, -180);
                gravityDown = false;
            }
        }
    }

    private void MovePlayer(float translation)
    {

        if (Utils.InfiniteScreenLogic(this.transform, maxX, maxY))
        {
            return;
        }

        if (!gravityDown)
        {
            translation *= -1;
        }

        if (translation == 0)
        {
            animator.SetBool("IsRunning", false);
        }
        else
        {
            animator.SetBool("IsRunning", true);
        }

        if (translation < 0 && direction == 1 || direction == 0)
        {
            direction = -1; transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        else if (translation > 0 && direction == -1 || direction == 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            direction = 1;
        }
        translation *= Time.deltaTime;
        transform.Translate(translation, 0, 0);

    }

    public void HeroAttack()
    {
        SoundManager.Instance.PlaySFX(Sounds.HIT_HERO);
        attackColider.enabled = true;
    }
    public void HeroAttackDone()
    {
        attackColider.enabled = false;
    }
    public void FireArrow()
    {
        SoundManager.Instance.PlaySFX(Sounds.BOW);
        //Vector3 position = new Vector3(this.transform.position.x + 2 * direction,this.transform.position.y,this.transform.position.z);

        var ArrowObj = ObjectPooler.instance.GetObjectFromPool(Tags.PROJECTILE);
        ArrowObj.transform.position = shootPoint.transform.position;
        //ArrowObj.transform.localScale = this.transform.localScale;


        Projectile prj = ArrowObj.GetComponent<Projectile>();
        prj.direction = direction;
        prj.isGravityDown = gravityDown;
        prj.maxX = maxX;
        prj.maxY = maxY;
        prj.playerHandler = PlayerHandler;
        ArrowObj.SetActive(true);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log(col.gameObject.tag);
        if (col.gameObject.tag == Tags.ENEMY_TOUCHABLE || col.gameObject.tag == Tags.BOSS)
        {
            PlayerHandler.EnemyDetected(!attackColider.enabled, col.gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == Tags.ENEMY || col.gameObject.tag == Tags.ENEMY_TOUCHABLE || col.gameObject.tag == Tags.BOSS)
        {
            PlayerHandler.AttackEnemyDetected(attackColider.enabled, col.gameObject);
        }

        if (col.gameObject.tag == Tags.DEATH_PLANE)
        {
            PlayerHandler.EnemyDetected(!attackColider.enabled, col.gameObject.transform.parent.gameObject);
        }
    }
}
