﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IEnemyHandler
{
    void EnemyDetected(bool isAttacking,Collider2D col);
}
public class Enemy : MonoBehaviour
{   
    public float speed = 10f;
    private GameObject player;
    private Collider2D fontAttackCollider;
    private Collider2D topAttackCollider;
    public int maxX = 0;
    public int maxY = 0;
    private Animator enemyAnimator;
    bool isGravityDown = true;
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Hero");
        speed = UnityEngine.Random.Range(5,10);
        enemyAnimator = GetComponent<Animator>();
        if(transform.childCount > 0){
        topAttackCollider = this.transform.GetChild(0)?.GetComponent<BoxCollider2D>();
        fontAttackCollider = this.transform.GetChild(1)?.GetComponent<BoxCollider2D>();
        }
    }

    void FixedUpdate()
    {
        Debug.Log(this.transform.localScale.x);
       
    }
    void LateUpdate(){
      
         if(player != null){
           CheckGravity();
            MoveToPlayerPosition();
           
        }
    }

    private void CheckGravity()
    {
         float physicsY = Physics2D.gravity.y;
              if(physicsY < 0){
                  
                   transform.rotation = Quaternion.Euler(0,0,0);
                   isGravityDown = true;
              }else{
                  
                   transform.rotation = Quaternion.Euler(0,0,-180);
                   isGravityDown = false;
              }
    }

    private void MoveToPlayerPosition()
    {
        if(Utils.InfiniteScreenLogic(this.transform,maxX,maxY)){
            return;
        }
        Vector3 vectorDiff = this.transform.position - player.transform.position;

       float dotValue = Vector3.Dot(Vector3.right,vectorDiff);

        float translation = 0;
        Vector3 scale = transform.localScale;
        if(dotValue <= 1 && dotValue >= -1){
            enemyAnimator.SetBool("Walking",false);
            enemyAnimator.SetTrigger("Attack");
        }else if (dotValue < 0){
          enemyAnimator.SetBool("Walking",true);
             if(scale.x > 0){
                scale.x *= -1;
            } 
            translation = 1;
            transform.localScale = new Vector3(scale.x,scale.y,scale.z);
        }else if (dotValue > 0){
            enemyAnimator.SetBool("Walking",true);
            translation = -1;
            if(scale.x < 0){
                scale.x *= -1;
            }  
             transform.localScale = new Vector3(scale.x,scale.y,scale.z);
        }
        
        if(!isGravityDown){
            translation *= -1;
            scale.x *= -1;
            transform.localScale = new Vector3(scale.x,scale.y,scale.z);
        }
        translation *= speed * Time.deltaTime;
        this.transform.Translate(translation,0,0);
    }

    void EnemyAttackBegin(){
        SoundManager.Instance.PlaySFX(Sounds.HIT_ENEMY);
       fontAttackCollider.enabled = true;
       topAttackCollider.enabled = true;
        
    }
    void EnemyAttackDone(){
       fontAttackCollider.enabled = false;
       topAttackCollider.enabled = false;
    }

  
}
