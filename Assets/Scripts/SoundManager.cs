﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static AudioClip bow, die, hitEnemy, hitHero, soundTrack1, soundTrack2, switchGravity, crabOst;

    private AudioSource AudioSourceSoundtrack;
    private AudioSource AudioSourceSfx;

    public static SoundManager Instance;

    // Start is called before the first frame update
    private void Awake()
    {
        if (!Instance)
        {
            bow = Resources.Load<AudioClip>(StaticRes.BOW_SOUND);
            die = Resources.Load<AudioClip>(StaticRes.DIE_SOUND);
            hitEnemy = Resources.Load<AudioClip>(StaticRes.HIT_ENEMY_SOUND);
            hitHero = Resources.Load<AudioClip>(StaticRes.HIT_HERO_SOUND);
            soundTrack1 = Resources.Load<AudioClip>(StaticRes.SOUNDTRACK_1);
            soundTrack2 = Resources.Load<AudioClip>(StaticRes.SOUNDTRACK_2);
            switchGravity = Resources.Load<AudioClip>(StaticRes.SWITCH_GRAVITY_SOUND);
            crabOst = Resources.Load<AudioClip>(StaticRes.SOUNDTRACK_CRAB);
            AudioSource[] sources = GetComponents<AudioSource>();

            AudioSourceSoundtrack = sources[0];
            AudioSourceSfx = sources[1];
            AudioSourceSfx.volume = StorageManager.GetSFXVolume();
            AudioSourceSoundtrack.volume = StorageManager.GetMusicVolume();

            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }


    }

    public void PlaySound(Sounds sound)
    {

        AudioSourceSoundtrack.Stop();

        switch (sound)
        {
            case Sounds.OST:
                AudioSourceSoundtrack.PlayOneShot(soundTrack1);
                break;
            case Sounds.OST2:
                AudioSourceSoundtrack.PlayOneShot(soundTrack2);
                break;
            case Sounds.SWITCH_GRAVITY:
                AudioSourceSoundtrack.PlayOneShot(switchGravity);
                break;
            case Sounds.CRAB_OST:
                AudioSourceSoundtrack.PlayOneShot(crabOst);
                break;
            default:
                break;
        }
    }
    public void PlaySFX(Sounds sounds)
    {
        switch (sounds)
        {
            case Sounds.BOW:
                AudioSourceSfx.PlayOneShot(bow);
                break;
            case Sounds.DIE:
                AudioSourceSfx.PlayOneShot(die);
                break;
            case Sounds.HIT_ENEMY:
                AudioSourceSfx.PlayOneShot(hitEnemy);
                break;
            case Sounds.HIT_HERO:
                AudioSourceSfx.PlayOneShot(hitHero);
                break;
        }
    }

    public void SetVolumeSFX(float volume)
    {
        AudioSourceSfx.volume = volume;
    }
    public void SetVolumeSound(float volume)
    {
        AudioSourceSoundtrack.volume = volume;
    }
}
