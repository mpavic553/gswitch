﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBossHandler
{
    void BossDestoryed(Vector3 position);
}
public class BossEnemy : Enemy
{

   public IBossHandler IBossHandler;

   
   void OnDisable(){
    IBossHandler.BossDestoryed(this.transform.position);
   }

    void OnCollisionEnter2D(Collision2D col){
        if(col.gameObject.tag == Tags.FLOATING_WALL){
            SoundManager.Instance.PlaySFX(Sounds.HIT_HERO);
            GameObject obj = ObjectPooler.instance.GetObjectFromPool(Tags.PARTICLE_SYSTEM);
            ParticleSystem.MainModule system = obj.GetComponent<ParticleSystem>().main;
            system.startColor = Color.gray;
            obj.transform.position = col.gameObject.transform.position;
            obj.SetActive(true);
            Destroy(col.gameObject);
        }
    }
}
