﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public IPlayerHandler playerHandler;

    public int maxX = 0;
    public int maxY = 0;

    private float speed = 20f;
    public bool isGravityDown;
    public int direction;

    // Start is called before the first frame update
    void OnEnable()
    {
        Debug.Log("Enabled");
        if (isGravityDown)
        {
            if (direction < 0 && transform.localScale.x > 0 || direction > 0 && transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            }
        }
        else
        {
            if (direction > 0 && transform.localScale.x > 0 || direction < 0 && transform.localScale.x < 0)
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            }
        }

    }
    // Update is called once per frame
    void Update()
    {
        if (Utils.InfiniteScreenLogic(this.transform, maxX, maxY))
        {
            return;
        }
        float translation = direction * speed * Time.deltaTime;

        if (!isGravityDown)
        {
            translation *= -1;
        }

        this.transform.Translate(translation, 0, 0);
    }

    private int FindDirection()
    {
        return (int)transform.localScale.x;
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == Tags.ENEMY || col.gameObject.tag == Tags.ENEMY_TOUCHABLE || col.gameObject.tag == Tags.BOSS)
        {
            playerHandler.AttackEnemyDetected(true, col.gameObject);
            col.gameObject.SetActive(false);
            var Enemy = col.gameObject.GetComponent<Enemy>();
            if (Enemy)
            {
                Destroy(Enemy);
            }
        }
        this.transform.localScale = new Vector3(1, 1, 1);
        this.direction = 1;
        this.isGravityDown = true;
        this.gameObject.SetActive(false);
    }
}
