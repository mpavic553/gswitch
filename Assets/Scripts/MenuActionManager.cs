﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuActionManager : MonoBehaviour
{

    public Slider SliderSFX;
    public Slider sliderSound;

    public GameObject Settings;
    public GameObject HowToPlay;
    public GameObject LeaderBoards;



    public void StartGameClick()
    {
        SceneManager.LoadScene("MainScene");
    }
    public void ExitGameClick()
    {
        Application.Quit();
    }

    public void SfxVolumeChange()
    {
        var value = SliderSFX.value;
        SoundManager.Instance.SetVolumeSFX(value);
    }
    public void SoundVolumeChange()
    {
        var value = sliderSound.value;
        SoundManager.Instance.SetVolumeSound(value);
    }

    public void GoBackFromSettings()
    {
        StorageManager.StoreMusicVolume(sliderSound.value);
        StorageManager.StoreSFXVolume(SliderSFX.value);
        GoBack(Settings);
    }
    public void OpenSettings()
    {
        var volumeSound = StorageManager.GetMusicVolume();
        var volumeSFX = StorageManager.GetSFXVolume();
        sliderSound.value = volumeSound;
        SliderSFX.value = volumeSFX;
        Settings.SetActive(true);
    }
    void Start()
    {
        SoundManager.Instance.PlaySound(Sounds.OST2);
    }

    public void OpenHowToPlay()
    {
        HowToPlay.SetActive(true);
    }
    public void OpeLeaderBoards()
    {
        LeaderBoards.SetActive(true);
    }

    public void GoBack(GameObject GameObject)
    {
        GameObject.SetActive(false);
    }
}


