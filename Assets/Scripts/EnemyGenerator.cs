﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public interface IEnemyGenerateHandler
{
    void EnemiesGone();
    void EnemiesRemaining(int remainingEnemies);
}
public class EnemyGenerator : MonoBehaviour, IBossHandler
{
    public IEnemyGenerateHandler enemyGenerateHandler;

    public LevelGenerator levelGenerator;

    private const int enemySpawnRate = 10;

   private int EnemyCount = 0;

   public void GenerateEnemies(int enemyCount = enemySpawnRate){
        
            SpawnEnemies(enemyCount,Vector3.zero);
            CountEnemies();
   }
    public void CountEnemies(){
         Debug.Log("REMAINING "+ EnemyCount);
       if(EnemyCount <= 0){
          enemyGenerateHandler.EnemiesGone();
       }else{
          enemyGenerateHandler.EnemiesRemaining(EnemyCount);
       }
    }

    public void EnemyDelete(){
       EnemyCount--;
       SyncEnemyCount();
    }
   private void SyncEnemyCount(){
      GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tags.ENEMY);
      GameObject[] enemiesTouch = GameObject.FindGameObjectsWithTag(Tags.ENEMY_TOUCHABLE);
      GameObject[] bosses = GameObject.FindGameObjectsWithTag(Tags.BOSS);

      var enemycount = enemies.Length + enemiesTouch.Length + bosses.Length;

      EnemyCount = enemycount;
   }
    private void SpawnEnemies(int enemyCount,Vector3 position)
    {
        
         foreach (GameObject enemy in ObjectPooler.instance.GetObjectsFromPool(enemyCount,(tag)=>{return tag.tag == Tags.ENEMY || tag.tag == Tags.ENEMY_TOUCHABLE;}))
         {
            Debug.Log("TAAAG: "+enemy.tag);
            float x = 0;
            float y = 0;
            if(position == Vector3.zero){
               int verticalLimit = levelGenerator.heightInUnits /2;
               int horizontallimit =levelGenerator.widthInUnits /2;
               x = UnityEngine.Random.Range(horizontallimit,horizontallimit+10);
               y = UnityEngine.Random.Range(verticalLimit,verticalLimit+10);
            }else
            {
               x = UnityEngine.Random.Range(position.x,position.x);
               y = UnityEngine.Random.Range(position.y,position.y + 3);
            } 
            enemy.transform.position = new Vector3(x,y,transform.position.z);
            Enemy em = enemy.AddComponent(typeof(Enemy)) as Enemy;
            em.maxX = levelGenerator.widthInUnits;
            em.maxY = levelGenerator.heightInUnits;
            enemy.SetActive(true);
            EnemyCount++;
      }  


      Debug.Log("REMAINING ENEMIES"+ EnemyCount);

    }

    public void SpawnBoss(){
       int x = UnityEngine.Random.Range(30,levelGenerator.widthInUnits - 30);
       int y = UnityEngine.Random.Range(30,levelGenerator.heightInUnits - 30);
       GameObject boss =  ObjectPooler.instance.GetObjectFromPool(Tags.ENEMY);
         Vector3 localScale = boss.transform.localScale;
         localScale.y += 2;
         if(localScale.x < 0){
            localScale.x = (localScale.x * -1) + 2;
         }else{
            localScale.x += 2;
         }
         boss.transform.localScale = localScale;
      boss.tag = Tags.BOSS;
       BossEnemy bossScript = boss.AddComponent(typeof(BossEnemy)) as BossEnemy;
       bossScript.IBossHandler = this;
         boss.transform.position = new Vector3(x,y,0);
       boss.SetActive(true);
       EnemyCount++;
    }

    public void BossDestoryed(Vector3 position)
    {
        SpawnEnemies(GameRules.BOSS_SPLIT_COUNT,position);
        CountEnemies();
    }


    public void RemoveEnemies(){
      GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tags.ENEMY);
      GameObject[] enemiesTouch = GameObject.FindGameObjectsWithTag(Tags.ENEMY_TOUCHABLE);
      GameObject[] bosses = GameObject.FindGameObjectsWithTag(Tags.BOSS);
      foreach (var enemy in enemies)
      {
         enemy.SetActive(false);
      }
      foreach (var enemy in enemiesTouch)
      {
         enemy.SetActive(false);
      }

      foreach (var boss in bosses)
      {
         boss.SetActive(false);
      }
    }
}
