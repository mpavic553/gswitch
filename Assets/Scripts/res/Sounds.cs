public enum Sounds {
    BOW = 1,
    DIE = 2,
    HIT_ENEMY = 3,
    HIT_HERO = 4,
    OST = 5,
    SWITCH_GRAVITY = 6,
    OST2 = 7,
    CRAB_OST = 8,
    
}