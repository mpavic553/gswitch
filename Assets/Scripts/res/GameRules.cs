public static class GameRules{
    public static int INITIAL_ENEMY_COUNT =1;
    public static int INCREMENT_PER_WAVE = 1;

    public static int INITIAL_ENEMY_WORTH = 5;
    public static int INCREMENT_ENEMY_WORTH = 5;
    
    public static int BOSS_SPLIT_COUNT = 5;
    public static int BOSS_TRESHHOLD =2 ;

}