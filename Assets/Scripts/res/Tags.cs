public class Tags {
    public  const string ENEMY = "Enemy";
     public  const string ENEMY_TOUCHABLE = "EnemyTouchable";
     public  const string DEATH_PLANE = "DeathPlane";
     public  const string BOSS = "Boss";
     public  const string PROJECTILE = "Projectile";
     public  const string FLOATING_WALL = "FloatingWall";
     public  const string PARTICLE_SYSTEM = "ParticleSystem";
}