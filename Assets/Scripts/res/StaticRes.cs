public static class StaticRes{
    public static string HERO_PATH = "Prefabs/Hero";
    public static string BOW_SOUND = "Sounds/Bow";
    public static string DIE_SOUND = "Sounds/Die";
    public static string HIT_ENEMY_SOUND = "Sounds/HitEnemy";
    public static string HIT_HERO_SOUND = "Sounds/HitHero";
    public static string SOUNDTRACK_1 = "Sounds/OST1";
    public static string SOUNDTRACK_2 = "Sounds/OST2";
    public static string SOUNDTRACK_CRAB = "Sounds/CrabOST"; 
    public static string SWITCH_GRAVITY_SOUND = "Sounds/SwitchGravity";
    public static string ARROW = "Prefabs/Arrow";
}