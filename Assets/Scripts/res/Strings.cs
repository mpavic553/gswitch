public static class Strings {
    public static string ENEMIES = "Enemies";
    public static string SCORE = "Score";
    public static string WAVE = "Wave";
    public static string BOSS_ROUND = "BOSS ROUND";
     public static string GAME_OVER = "Game Over";
     public static string TRY_AGAIN = "Try Again";
}