using UnityEngine;
public class Utils{


    public static bool InfiniteScreenLogic(Transform transform,int maxX,int maxY ){
        if(maxX == 0 || maxY ==0){
             return false;
        }
        if(transform.position.x > maxX){
            if(transform.position.y > maxY){
                transform.position = new Vector3(1,1,transform.position.z);
            }else{
                transform.position = new Vector3(1,transform.position.y, transform.position.z);
            }
          return true;
         }

         if(transform.position.x <= 0){
              if(transform.position.y < 0){
                transform.position = new Vector3(maxX-1,maxY-1,transform.position.z);
            }else{
                transform.position = new Vector3(maxX-1,transform.position.y, transform.position.z);
            }
         }
         return false;
    }
}