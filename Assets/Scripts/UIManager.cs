﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public interface IUIManage
{
    void PrintScore(int score);
    void PrintWave(int wave);
    void PrintBossRound();
    void PrintEnemyCount(int enemyCount);

    void PrintGameOver();
}
public class UIManager : MonoBehaviour, IUIManage
{

    public Text ScoreText;
    public Text WaveText;
    public Text EnemyCountText;
    public Text PauseTitle;
    public Text ButtonConfirmText;
    public InputField Nickname;
    public GameObject PauseMenu;
    public GameObject SaveScoreForm;
    public Button SaveScoreButton;

    private bool isPaused = false;
    private bool isGameOver = false;
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && !isGameOver)
        {
            isPaused = !isPaused;
        }
        SetPause();
    }

    private void SetPause()
    {
        PauseMenu.SetActive(isPaused);

        Time.timeScale = isPaused ? 0 : 1;
    }

    public void PrintEnemyCount(int enemyCount)
    {
        EnemyCountText.text = Strings.ENEMIES + ": " + enemyCount;
    }

    public void PrintGameOver()
    {
        WaveText.text = Strings.GAME_OVER;
        PauseTitle.text = Strings.GAME_OVER;
        ButtonConfirmText.text = Strings.TRY_AGAIN;
        isPaused = true;
        SetPause();
        isGameOver = true;
        SaveScoreForm.SetActive(true);
    }

    public void PrintScore(int score)
    {
        ScoreText.text = Strings.SCORE + ": " + score;
    }

    public void PrintWave(int wave)
    {
        WaveText.text = Strings.WAVE + ": " + wave;
    }
    public void PrintBossRound()
    {
        WaveText.text = Strings.BOSS_ROUND;
    }

    public void ResumeClick()
    {
        if (!isGameOver)
        {
            isPaused = false;
            SetPause();
        }
        else
        {
            ObjectPooler.instance.Clear();
            SceneManager.LoadScene("MainScene");
            SoundManager.Instance.PlaySound(Sounds.OST2);
        }

    }

    public void ExitClick()
    {
        ObjectPooler.instance.Clear();
        SceneManager.LoadScene("TitlePageScreen");
    }

    public void SaveScore()
    {
        HighScoreManager.Instance.SaveScore(Nickname.text);
        SaveScoreButton.enabled = false;
        HighScoreManager.Instance.SaveChanges();
    }
}
